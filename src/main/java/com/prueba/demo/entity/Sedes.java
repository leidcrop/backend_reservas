package com.prueba.demo.entity;
import java.util.Date;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

@Entity
@Table(name="sedes")
public class Sedes {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;

	

	@Column(name = "nombre")
	private String nombre;


	@Column(name = "ubicacion")
	private String ubicacion;

	




	public Sedes() {
	}

	public Sedes(int id,String nombre,String ubicacion) {
		this.id = id;
		this.nombre = nombre;
		this.ubicacion = ubicacion;
		

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}



	public String getUbicacion() {
		return ubicacion;
	}

	public void setUbicacion(String ubicacion) {
		this.ubicacion = ubicacion;
	}

	
	




	@Override
	public String toString() {
		return "Sedes [id=" + id + ",  nombre=" + nombre + ",ubicacion=" + ubicacion  + "]";
	}

}