package com.prueba.demo.entity;
import java.util.Date;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

@Entity
@Table(name="salones")
public class Salones {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;

	

	@Column(name = "nombre")
    private String nombre;
    
    @Column(name = "id_sede")
	private int id_sede;




	




	public Salones() {
	}

	public Salones(int id,String nombre,int id_sede) {
		this.id = id;
        this.nombre = nombre;
        this.id_sede = id_sede;

	
		

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
    }
    
    public int getId_sede() {
		return id_sede;
	}

	public void setId_sede(int id_sede) {
		this.id_sede = id_sede;
	}




	
	




	@Override
	public String toString() {
		return "Salones [id=" + id + ",  nombre=" + nombre +  ",id_sede=" + id_sede + "]";
	}

}