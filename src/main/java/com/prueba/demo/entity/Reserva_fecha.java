package com.prueba.demo.entity;
import java.util.Date;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

@Entity
@Table(name="reserva_fecha")
public class Reserva_fecha {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;

	



    @Column(name = "fecha_reserva")
    @CreationTimestamp
	private Date  fecha_reserva;


	@Column(name = "id_reserva")
	private int id_reserva;

	




	public Reserva_fecha() {
	}

	public Reserva_fecha(int id,int id_reserva) {
		this.id = id;
		this.id_reserva = id_reserva;
		

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}


	



	public Date getFecha_reserva() {
		return fecha_reserva;
	}

	public void setFecha_reserva(Date fecha_reserva) {
		this.fecha_reserva = fecha_reserva;
	}


	public int getId_reserva() {
		return id_reserva;
	}

	public void setid_reserva(int id_reserva) {
		this.id_reserva = id_reserva;
	}
	
	




	@Override
	public String toString() {
		return "Reserva_fecha [id=" + id + ",   fecha_reserva="
				+ fecha_reserva + ",id_reserva=" + id_reserva  + "]";
	}

}