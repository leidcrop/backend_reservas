package com.prueba.demo.entity;
import java.sql.Time;
import java.util.Date;
import java.util.Timer;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

@Entity
@Table(name="reservas")
public class Reservas {

@Id
@GeneratedValue(strategy = GenerationType.IDENTITY)
@Column(name = "id")
private int id;

@Column(name = "codigo")
private String codigo;



@Column(name = "fecha_creacion")
@CreationTimestamp
private Date  fecha_creacion;

@Column(name = "hora_inicio")
@CreationTimestamp
private Time hora_inicio;

@Column(name = "hora_fin")
@CreationTimestamp
private Time hora_fin;

@Column(name = "id_videobeam")
private int id_videobeam;

@Column(name = "id_usuario")
private int id_usuario;

@Column(name = "id_salon")
private int id_salon;

@Column(name = "id_frecuencia")
private int id_frecuencia;
@Column(name = "id_periodo")
private int id_periodo;



public Reservas() {
}

public Reservas(int id, String codigo, int id_videobeam,int id_usuario, int id_salon,int id_frecuencia,int id_periodo) {
this.id = id;
this.codigo = codigo;
this.id_videobeam = id_videobeam;
this.id_usuario = id_usuario;
this.id_salon= id_salon;
this.id_salon= id_frecuencia;
this.id_periodo=id_periodo;
}


public int getId() {
return id;
}

public void setId(int id) {
this.id = id;
}

public String getcodigo(String codigo) {
return codigo;
}

public void setCodigo(String codigo) {
this.codigo = codigo;
}



public Date getFecha_creacion() {
    return fecha_creacion;
}

public void setFecha_creacion(Date fecha_creacion) {
    this.fecha_creacion = fecha_creacion;
}

public Time getHora_inicio() {
    return hora_inicio;
}

public void setHora_inicio(Time hora_inicio) {
    this.hora_inicio = hora_inicio;

}public Date getHora_fin() {
    return hora_fin;
}

public void setHora_fin(Time hora_fin) {
    this.hora_fin = hora_fin;
}


public int getId_videobeam() {
return id_videobeam;
}

public void setId_videobeam(int id_videobeam) {
this.id_videobeam = id_videobeam;
}


public int getId_usuario() {
return id_usuario;
}

public void setId_usuario(int id_usuario) {
this.id_usuario = id_usuario;
}


public int getId_salon() {
return id_salon;
}

public void setId_salon(int id_salon) {
this.id_salon = id_salon;
}


public int getId_frecuencia() {
return id_frecuencia;
}

public void setId_frecuencia(int id_frecuencia) {
this.id_frecuencia = id_frecuencia;
}


public int getId_periodo() {
return id_periodo;
}

public void setId_periodo(int id_periodo) {
this.id_periodo = id_periodo;
}






@Override
public String toString() {
return "Reservas [id=" + id + ", Codigo=" + codigo + ", fecha_creacion=" + fecha_creacion + ", hora_inicio="
+ hora_inicio + ", hora_fin=" + hora_fin + ",id_videobeam=" + id_videobeam + ",id_usuario=" + id_usuario + ",id_salon="
+ id_salon +",id_frecuencia=" + id_frecuencia + ",id_periodo=" + id_periodo + "]";
}

}

