package com.prueba.demo.entity;
import java.util.Date;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

@Entity
@Table(name="roles")
public class Roles {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;

	

	@Column(name = "nombre")
	private String nombre;


	@Column(name = "descripcion")
	private String descripcion;

	




	public Roles() {
	}

	public Roles(int id,String nombre,String descripcion) {
		this.id = id;
		this.nombre = nombre;
		this.descripcion = descripcion;
		

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}



	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	
	




	@Override
	public String toString() {
		return "Roles [id=" + id + ",  nombre=" + nombre + ",descripcion=" + descripcion  + "]";
	}

}