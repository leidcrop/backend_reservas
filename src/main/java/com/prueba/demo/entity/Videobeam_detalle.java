package com.prueba.demo.entity;
import java.util.Date;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

@Entity
@Table(name="videobeam_detalle")
public class Videobeam_detalle {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;

	

	@Column(name = "marca")
    private String marca;

    @Column(name = "serie")
    private String serie;
    
    @Column(name = "caracteristicas")
    private String caracteristicas;
    

	public Videobeam_detalle() {
	}

	public Videobeam_detalle(int id,String marca,String serie,String caracteristicas) {
		this.id = id;
        this.marca = marca;
        this.serie = serie;
        this.serie = caracteristicas;


	
		

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
    }
    
    public String getSerie() {
		return serie;
	}

	public void setSerie(String serie) {
		this.serie = serie;
    }
    

    public String getCarateristicas() {
		return caracteristicas;
	}

	public void setCarateristicas(String carateristicas) {
		this.caracteristicas = carateristicas;
    }
    




	
	




	@Override
	public String toString() {
        return "Videobeam_detalle [id=" + id + ",  marca=" + marca +  ",serie=" + serie + ",  carateristicas=" + caracteristicas + "]";
    
    }

}