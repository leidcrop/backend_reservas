package com.prueba.demo.entity;
import java.util.Date;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

@Entity
@Table(name="periodos")
public class Periodos {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;

	

	@Column(name = "nombre")
	private String nombre;

    @Column(name = "fecha_inicio")
    @CreationTimestamp
	private Date  fecha_inicio;

    @Column(name = "fecha_fin")
    @CreationTimestamp
    private Date fecha_fin;

	@Column(name = "estado")
	private String estado;

	




	public Periodos() {
	}

	public Periodos(int id,String nombre,String estado) {
		this.id = id;
		this.nombre = nombre;
		this.estado = estado;
		

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}



	public Date getFecha_inicio() {
		return fecha_inicio;
	}

	public void setFecha_inicio(Date fecha_inicio) {
		this.fecha_inicio = fecha_inicio;
	}

	public Date getFecha_fin() {
		return fecha_fin;
	}

	public void setFecha_fin(Date fecha_fin) {
		this.fecha_fin = fecha_fin;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	
	




	@Override
	public String toString() {
		return "Periodos [id=" + id + ",  nombre=" + nombre + ", fecha_inicio="
				+ fecha_inicio + ",fecha_fin=" + fecha_fin + ",estado=" + estado  + "]";
	}

}