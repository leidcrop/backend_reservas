package com.prueba.demo.entity;
import java.util.Date;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

@Entity
@Table(name="frecuencias")
public class Frecuencia {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;

	

	@Column(name = "nombre")
	private String nombre;



	




	public Frecuencia() {
	}

	public Frecuencia(int id,String nombre) {
		this.id = id;
		this.nombre = nombre;
	
		

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}




	
	




	@Override
	public String toString() {
		return "Frecuencia [id=" + id + ",  nombre=" + nombre +  "]";
	}

}