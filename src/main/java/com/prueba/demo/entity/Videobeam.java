package com.prueba.demo.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="videobeam")
public class Videobeam {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;

	

	@Column(name = "codigo")
	private int codigo;

	@Column(name = "observaciones")
	private String observaciones;

	@Column(name = "id_videobeam")
    private int id_videobeam;

	@Column(name = "id_sede")
	private int id_sede;

	
	public Videobeam() {
	}

	public Videobeam(int id,int codigo,String observaciones,int id_videobeam,int id_sede) {
		this.id = id;
		this.codigo = codigo;
        this.observaciones = observaciones;
        this.id_videobeam = id_videobeam;
		this.id_sede = id_sede;

		

	}
    
    


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	


	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
    }
    
    public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}


	public int getId_videobeam() {
		return id_videobeam;
	}

	public void setId_videobeam(int id_videobeam) {
		this.id_videobeam = id_videobeam;
    }
    
    public int getId_sede() {
		return id_sede;
	}

	public void setId_sede(int id_sede) {
		this.id_sede = id_sede;
	}


	
	
	




	@Override
	public String toString() {
		return "Videobeam [id=" + id + " , codigo=" + codigo+ ", observaciones="
				+ observaciones + ",id_videobeam=" + id_videobeam + ",id_sede=" + id_sede + "]";
	}

}