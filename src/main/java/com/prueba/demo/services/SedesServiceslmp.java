package com.prueba.demo.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.prueba.demo.dao.SedeDAO;
import com.prueba.demo.entity.Sedes;

@Service
public class SedesServiceslmp implements SedesService {

	@Autowired
	private SedeDAO sedeDAO;

	@Override
	public List<Sedes> findAll() {
		List<Sedes> listSedes = sedeDAO.findAll();
		return listSedes;
	}

	@Override
	public Sedes findById(int id) {
		Sedes sedes = sedeDAO.findById(id);
		return sedes;
	}



	

}