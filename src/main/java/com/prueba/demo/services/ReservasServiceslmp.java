package com.prueba.demo.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.prueba.demo.dao.ReservaDao;
import com.prueba.demo.entity.Reservas;

@Service
public class ReservasServiceslmp implements ReservaService {

	@Autowired
	private ReservaDao reservaDao;

	@Override
	public List<Reservas> findAll() {
		List<Reservas> listReserva = reservaDao.findAll();
		return listReserva;
	}

	@Override
	public Reservas findById(int id) {
		Reservas reservas = reservaDao.findById(id);
		return reservas;
	}


	@Override
	public void save(Reservas reservas) {
		reservaDao.save(reservas);

	}

	@Override
	public void deleteById(int id) {
		reservaDao.deleteById(id);
	}


	

}