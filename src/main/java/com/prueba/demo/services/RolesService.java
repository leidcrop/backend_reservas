package com.prueba.demo.services;

import java.util.List;

import com.prueba.demo.entity.Roles;

public interface RolesService {
	
	public List<Roles> findAll();
	
	public Roles findById(int id);


}