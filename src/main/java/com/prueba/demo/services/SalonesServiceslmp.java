package com.prueba.demo.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.prueba.demo.dao.SalonesDao;
import com.prueba.demo.entity.Salones;

@Service
public class SalonesServiceslmp implements SalonesServices {

	@Autowired
	private SalonesDao salonesDAO;

	@Override
	public List<Salones> findAll() {
		List<Salones> listSalones = salonesDAO.findAll();
		return listSalones;
	}

	@Override
	public Salones findById(int id) {
		Salones salones = salonesDAO.findById(id);
		return salones;
	}



	

}