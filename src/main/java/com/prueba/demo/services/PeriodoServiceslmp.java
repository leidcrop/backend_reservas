package com.prueba.demo.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.prueba.demo.dao.PeriodoDao;
import com.prueba.demo.entity.Periodos;

@Service
public class PeriodoServiceslmp implements PeriodoService {

	@Autowired
	private PeriodoDao periodoDao;

	@Override
	public List<Periodos> findAll() {
		List<Periodos> ListPeriodos = periodoDao.findAll();
		return ListPeriodos;
	}

	@Override
	public Periodos findById(int id) {
		Periodos periodos = periodoDao.findById(id);
		return periodos;
	}



	@Override
	public void save(Periodos periodos) {
		periodoDao.save(periodos);

	}

	@Override
	public void deleteById(int id) {
		periodoDao.deleteById(id);
	}
	

}