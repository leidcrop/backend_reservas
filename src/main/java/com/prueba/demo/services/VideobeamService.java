package com.prueba.demo.services;

import java.util.List;

import com.prueba.demo.entity.Videobeam;

public interface VideobeamService {
	
	public List<Videobeam> findAll();
	
	public Videobeam findById(int id);


}