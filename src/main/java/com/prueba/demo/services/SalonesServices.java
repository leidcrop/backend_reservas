package com.prueba.demo.services;

import java.util.List;

import com.prueba.demo.entity.Salones;

public interface SalonesServices {
	
	public List<Salones> findAll();
	
	public Salones findById(int id);


}