package com.prueba.demo.services;

import java.util.List;

import com.prueba.demo.entity.Periodos;

public interface PeriodoService {
	
	public List<Periodos> findAll();
	
	public Periodos findById(int id);

	public void save(Periodos periodos);
	
	public void deleteById(int id);
}