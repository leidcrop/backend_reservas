package com.prueba.demo.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.prueba.demo.dao.Reserva_fechaDao;
import com.prueba.demo.entity.Reserva_fecha;

@Service
public class Reserva_fechaServiceslmp implements Reserva_fechaService {

	@Autowired
	private Reserva_fechaDao reserva_fechaDao;

	@Override
	public List<Reserva_fecha> findAll() {
		List<Reserva_fecha> listReserva_fecha = reserva_fechaDao.findAll();
		return listReserva_fecha;
	}

	@Override
	public Reserva_fecha findById(int id) {
		Reserva_fecha reserva_fecha = reserva_fechaDao.findById(id);
		return reserva_fecha;
	}

	@Override
	public void save(Reserva_fecha reserva_fecha) {
		reserva_fechaDao.save(reserva_fecha);

	}

	@Override
	public void deleteById(int id) {
		reserva_fechaDao.deleteById(id);
	}



	

}