package com.prueba.demo.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.prueba.demo.dao.RolesDao;
import com.prueba.demo.entity.Roles;

@Service
public class RolesServiceslmp implements RolesService {

	@Autowired
	private RolesDao rolesDao;

	@Override
	public List<Roles> findAll() {
		List<Roles> Listroles = rolesDao.findAll();
		return Listroles;
	}

	@Override
	public Roles findById(int id) {
		Roles roles = rolesDao.findById(id);
		return roles;
	}



	

}