package com.prueba.demo.services;

import java.util.List;

import com.prueba.demo.entity.User;

public interface UserServices {
	
	public List<User> findAll();
	
	public User findById(int id);
	
	public void save(User user);
	
	public void deleteById(int id);

	public User findBylogin(String correo ,String password);

}