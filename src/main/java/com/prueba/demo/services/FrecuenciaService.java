package com.prueba.demo.services;

import java.util.List;

import com.prueba.demo.entity.Frecuencia;

public interface FrecuenciaService {
	
	public List<Frecuencia> findAll();
	
	public Frecuencia findById(int id);


}