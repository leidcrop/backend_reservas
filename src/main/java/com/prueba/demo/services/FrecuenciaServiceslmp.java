package com.prueba.demo.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.prueba.demo.dao.FrecuenciaDao;
import com.prueba.demo.entity.Frecuencia;

@Service
public class FrecuenciaServiceslmp implements FrecuenciaService {

	@Autowired
	private FrecuenciaDao frecuenciadao;

	@Override
	public List<Frecuencia> findAll() {
		List<Frecuencia> listFrecuencia = frecuenciadao.findAll();
		return listFrecuencia;
	}

	@Override
	public Frecuencia findById(int id) {
		Frecuencia frecuencia = frecuenciadao.findById(id);
		return frecuencia;
	}



	

}