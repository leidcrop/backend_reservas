package com.prueba.demo.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.prueba.demo.dao.VideobeamDao;
import com.prueba.demo.entity.Videobeam;

@Service
public class VideobeamServiceslmp implements VideobeamService {

	@Autowired
	private VideobeamDao videobeamDao;

	@Override
	public List<Videobeam> findAll() {
		List<Videobeam> lVideobeams = videobeamDao.findAll();
		return lVideobeams;
	}

	@Override
	public Videobeam findById(int id) {
		Videobeam videobeam = videobeamDao.findById(id);
		return videobeam;
	}



	

}