package com.prueba.demo.services;

import java.util.List;

import com.prueba.demo.entity.Sedes;

public interface SedesService {
	
	public List<Sedes> findAll();
	
	public Sedes findById(int id);


}