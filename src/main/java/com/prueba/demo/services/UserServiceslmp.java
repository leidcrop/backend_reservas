package com.prueba.demo.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.prueba.demo.dao.UserDao;
import com.prueba.demo.entity.User;

@Service
public class UserServiceslmp implements UserServices {

	@Autowired
	private UserDao userDAO;

	@Override
	public List<User> findAll() {
		List<User> listUsers = userDAO.findAll();
		return listUsers;
	}

	@Override
	public User findById(int id) {
		User user = userDAO.findById(id);
		return user;
	}

	@Override
	public void save(User user) {
		userDAO.save(user);

	}

	@Override
	public void deleteById(int id) {
		userDAO.deleteById(id);
	}

	@Override
	public User findBylogin(String correo, String password) {
		User user = userDAO.findByLogin(correo, password);
		return user;
	}

}