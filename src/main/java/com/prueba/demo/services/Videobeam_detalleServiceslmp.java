package com.prueba.demo.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.prueba.demo.dao.Videobeam_detalleDao;
import com.prueba.demo.entity.Videobeam_detalle;

@Service
public class Videobeam_detalleServiceslmp implements Videobeam_detalleService {

	@Autowired
	private Videobeam_detalleDao videobeam_detalleDao;

	@Override
	public List<Videobeam_detalle> findAll() {
		List<Videobeam_detalle> lisVidebeamdetalle = videobeam_detalleDao.findAll();
		return lisVidebeamdetalle;
	}

	@Override
	public Videobeam_detalle findById(int id) {
		Videobeam_detalle videobeam_detalle = videobeam_detalleDao.findById(id);
		return videobeam_detalle;
	}



	

}