DROP TABLE roles;
create table roles (
id int AUTO_INCREMENT PRIMARY KEY,
nombre varchar(30) not null,
descripcion varchar (30) not null);


DROP TABLE usuarios;
create table usuarios (
id int AUTO_INCREMENT PRIMARY KEY,
identificacion int NOT NULL,
nombres varchar(30) NOT NULL,
apellidos varchar (30) NOT NULL,
telefono int NOT NULL,
correo varchar(30) NOT NULL,
passwor varchar(15) NOT NULL,
id_rol int NOT NULL);






DROP TABLE reservas;
create table reservas (
id int AUTO_INCREMENT PRIMARY KEY,
codigo varchar(6) NOT NULL,
fecha_creacion date ,
hora_inicio time ,
hora_fin time ,
id_videobeam int NOT NULL,
id_usuario int NOT NULL ,
id_salon int NOT NULL,
id_frecuencia int NOT NULL,
id_periodo int NOT NULL);



DROP TABLE videobeam_detalle;
create  table videobeam_detalle (
id int  AUTO_INCREMENT PRIMARY KEY,
marca varchar(30) NOT NULL,
serie  varchar(30)NOT NULL,
caracteristicas varchar(30) NOT NULL);



DROP TABLE videobeam;
create  table videobeam (
id integer  AUTO_INCREMENT PRIMARY KEY,
codigo varchar(6) NOT NULL,
observaciones varchar(30) NOT NULL,
id_videobeam int NOT NULL,
id_sede int NOT NULL);






DROP  TABLE salones;
create  table   salones(
id int not null AUTO_INCREMENT PRIMARY KEY,
nombre varchar(30) NOT NULL,
id_sede int NOT NULL);






DROP TABLE sedes;
create  table sedes (
 id int not null AUTO_INCREMENT PRIMARY KEY,
nombre varchar(30) NOT NULL,
ubicacion varchar (100) NOT NULL);




DROP TABLE frecuencias;
create  table frecuencias (
id int  AUTO_INCREMENT PRIMARY KEY,
nombre varchar(30) NOT NULL);




DROP TABLE reserva_fecha ;
create  table reserva_fecha(
id int  AUTO_INCREMENT PRIMARY KEY,
fecha_reserva Date NOT NULL,
id_reserva int NOT NULL);



DROP TABLE periodos;
create  table  periodos(
id int AUTO_INCREMENT PRIMARY KEY,
nombre varchar(7) NOT NULL,
fecha_inicio date NOT NULL,
fecha_fin date NOT NULL,
estado varchar(30) NOT NULL);







alter table usuarios  add constraint roles1  foreign key (id_rol) references roles (id);
alter table reservas add constraint vide1 foreign key (id_videobeam) references videobeam (id);
alter table reservas add constraint salo1  foreign key (id_salon) references salones (id);
alter table reservas add constraint frecu1  foreign key (id_frecuencia) references frecuencias (id);
alter table reservas add constraint docen1  foreign key (id_usuario) references usuarios (id);
alter table reservas add constraint peri1   foreign key (id_periodo) references periodos (id);
alter table videobeam add constraint vie01   foreign key (id_videobeam) references videobeam_detalle (id);
alter table videobeam add constraint vie02   foreign key (id_sede) references sedes (id);
alter table salones add constraint sede02   foreign key (id_sede) references sedes (id);
alter table reserva_fecha add constraint reser1   foreign key (id_reserva) references reservas (id);



INSERT roles (id, nombre, descripcion)    VALUES ( 1, 'Administrador', 'se encarga de administrar la recepcion de videobins');
INSERT roles (id, nombre, descripcion)    VALUES ( 2, 'Docente', 'se encarga de reservar los videobins');

INSERT frecuencias (id, nombre )    VALUES ( 1, 'Semanal');
INSERT frecuencias (id, nombre )    VALUES ( 2, 'Quincenal');


INSERT videobeam_detalle (id, marca, serie,caracteristicas )    VALUES ( 1, 'Samsumg', 'sa-110b','resitente al agua' );
INSERT videobeam_detalle (id, marca, serie,caracteristicas )    VALUES ( 2, 'Alcatel', 'AL-110b','resitente al agua' );

INSERT sedes (id, nombre, ubicacion )    VALUES ( 1, 'universidad antonio jose camacho sede sur ', 'Cl. 25 #127-220, Cali, Valle del Cauca' );
INSERT sedes (id, nombre, ubicacion )    VALUES ( 2, 'universidad antonio jose camacho sede norte ', 'Av. 6 Nte. #28 Norte102, Cali, Valle del Cauca' );


INSERT salones (id, nombre, id_sede )    VALUES ( 1, 'sl-110', 1 );
INSERT salones (id, nombre, id_sede )    VALUES ( 2, 'sl-111', 1 );
INSERT salones (id, nombre, id_sede )    VALUES ( 3, 'sl-112', 1 );
INSERT salones (id, nombre, id_sede )    VALUES ( 4, 'sl-113', 1 );
INSERT salones (id, nombre, id_sede )    VALUES ( 5, 'sl-114', 1 );
INSERT salones (id, nombre, id_sede )    VALUES ( 6, 'sn-210', 2 );
INSERT salones (id, nombre, id_sede )    VALUES ( 7, 'sn-211', 2 );
INSERT salones (id, nombre, id_sede )    VALUES ( 8, 'sn-212', 2 );
INSERT salones (id, nombre, id_sede )    VALUES ( 9, 'sn-213', 2 );
INSERT salones (id, nombre, id_sede )    VALUES ( 10, 'sn-214',2 );



INSERT usuarios (id, identificacion, nombres,apellidos,telefono,correo,passwor,id_rol)   VALUES ( 1, 32131312, 'carlos','vaca',4123123,'stwengil@hotmai.com','123',1);








