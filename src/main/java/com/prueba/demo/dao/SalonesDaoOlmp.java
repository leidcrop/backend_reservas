package com.prueba.demo.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.prueba.demo.entity.Salones;

@Repository
public class SalonesDaoOlmp implements SalonesDao {

	@Autowired
	private EntityManager entityManager;

	@Override
	public List<Salones> findAll() {
		Session currentSession = entityManager.unwrap(Session.class);

		Query<Salones> theQuery = currentSession.createQuery("from Salones", Salones.class);

		List<Salones> salones = theQuery.getResultList();

		return salones;
	}

	@Override
	public Salones findById(int id) {
		Session currentSession = entityManager.unwrap(Session.class);

		Salones salones = currentSession.get(Salones.class, id);

		return salones;
	}







}