package com.prueba.demo.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.prueba.demo.entity.Periodos;

@Repository
public class PeriodoDaoOlmp implements PeriodoDao {

	@Autowired
	private EntityManager entityManager;

	@Override
	public List<Periodos> findAll() {
		Session currentSession = entityManager.unwrap(Session.class);

		Query<Periodos> theQuery = currentSession.createQuery("from Periodos", Periodos.class);

		List<Periodos> periodos = theQuery.getResultList();

		return periodos;
	}

	@Override
	public Periodos findById(int id) {
		Session currentSession = entityManager.unwrap(Session.class);

		Periodos periodos = currentSession.get(Periodos.class, id);

		return periodos;
	}


	
	@Override
	public void save(Periodos periodos) {
		Session currentSession = entityManager.unwrap(Session.class);

		currentSession.saveOrUpdate(periodos);

	}

	@Override
	public void deleteById(int id) {
		Session currentSession = entityManager.unwrap(Session.class);

		Query<Periodos> theQuery = currentSession.createQuery("delete from Periodos where id=:idPeriodos", Periodos.class);

		theQuery.setParameter("idPeriodos", id);
		theQuery.executeUpdate();

	}






}