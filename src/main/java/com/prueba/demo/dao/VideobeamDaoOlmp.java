package com.prueba.demo.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.prueba.demo.entity.Videobeam;

@Repository
public class VideobeamDaoOlmp implements VideobeamDao {

	@Autowired
	private EntityManager entityManager;

	@Override
	public List<Videobeam> findAll() {
		Session currentSession = entityManager.unwrap(Session.class);

		Query<Videobeam> theQuery = currentSession.createQuery("from Videobeam", Videobeam.class);

		List<Videobeam> videobeam = theQuery.getResultList();

		return videobeam;
	}

	@Override
	public Videobeam findById(int id) {
		Session currentSession = entityManager.unwrap(Session.class);

		Videobeam videobeam = currentSession.get(Videobeam.class, id);

		return videobeam;
	}







}