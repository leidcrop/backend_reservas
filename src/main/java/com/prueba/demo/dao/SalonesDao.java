package com.prueba.demo.dao;

import java.util.List;

import com.prueba.demo.entity.Salones;

public interface SalonesDao {

	public List<Salones> findAll();

	public Salones findById(int id);


}