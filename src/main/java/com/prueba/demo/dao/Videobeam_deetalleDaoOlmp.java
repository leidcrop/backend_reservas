package com.prueba.demo.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.prueba.demo.entity.Videobeam_detalle;

@Repository
public class Videobeam_deetalleDaoOlmp implements Videobeam_detalleDao {

	@Autowired
	private EntityManager entityManager;

	@Override
	public List<Videobeam_detalle> findAll() {
		Session currentSession = entityManager.unwrap(Session.class);

		Query<Videobeam_detalle> theQuery = currentSession.createQuery("from Videobeam_detalle", Videobeam_detalle.class);

		List<Videobeam_detalle> videobeam_detalle = theQuery.getResultList();

		return videobeam_detalle;
	}

	@Override
	public Videobeam_detalle findById(int id) {
		Session currentSession = entityManager.unwrap(Session.class);

		Videobeam_detalle videobeam_detalle = currentSession.get(Videobeam_detalle.class, id);

		return videobeam_detalle;
	}







}