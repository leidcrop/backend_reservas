package com.prueba.demo.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.prueba.demo.entity.Reservas;

@Repository
public class ReservasDaoOlmp implements ReservaDao {

	@Autowired
	private EntityManager entityManager;

	@Override
	public List<Reservas> findAll() {
		Session currentSession = entityManager.unwrap(Session.class);

		Query<Reservas> theQuery = currentSession.createQuery("from Reservas", Reservas.class);

		List<Reservas> reservas = theQuery.getResultList();

		return reservas;
	}

	@Override
	public Reservas findById(int id) {
		Session currentSession = entityManager.unwrap(Session.class);

		Reservas reservas = currentSession.get(Reservas.class, id);

		return reservas;
	}


	@Override
	public void save(Reservas reservas) {
		Session currentSession = entityManager.unwrap(Session.class);

		currentSession.saveOrUpdate(reservas);

	}

	@Override
	public void deleteById(int id) {
		Session currentSession = entityManager.unwrap(Session.class);

		Query<Reservas> theQuery = currentSession.createQuery("delete from Reservas where id=:idReservas", Reservas.class);

		theQuery.setParameter("idReservas", id);
		theQuery.executeUpdate();

	}






}