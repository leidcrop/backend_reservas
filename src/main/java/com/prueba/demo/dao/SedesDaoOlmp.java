package com.prueba.demo.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.prueba.demo.entity.Sedes;

@Repository
public class SedesDaoOlmp implements SedeDAO {

	@Autowired
	private EntityManager entityManager;

	@Override
	public List<Sedes> findAll() {
		Session currentSession = entityManager.unwrap(Session.class);

		Query<Sedes> theQuery = currentSession.createQuery("from Sedes", Sedes.class);

		List<Sedes> sedes = theQuery.getResultList();

		return sedes;
	}

	@Override
	public Sedes findById(int id) {
		Session currentSession = entityManager.unwrap(Session.class);

		Sedes sedes = currentSession.get(Sedes.class, id);

		return sedes;
	}







}