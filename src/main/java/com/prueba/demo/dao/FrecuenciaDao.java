package com.prueba.demo.dao;

import java.util.List;

import com.prueba.demo.entity.Frecuencia;

public interface FrecuenciaDao {

	public List<Frecuencia> findAll();

	public Frecuencia findById(int id);


}