package com.prueba.demo.dao;

import java.util.List;

import com.prueba.demo.entity.Periodos;

public interface PeriodoDao {

	public List<Periodos> findAll();

	public Periodos findById(int id);

	public void save(Periodos periodos);

	public void deleteById(int id);


}