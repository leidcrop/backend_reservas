package com.prueba.demo.dao;

import java.util.List;

import com.prueba.demo.entity.Videobeam;

public interface VideobeamDao {

	public List<Videobeam> findAll();

	public Videobeam findById(int id);


}