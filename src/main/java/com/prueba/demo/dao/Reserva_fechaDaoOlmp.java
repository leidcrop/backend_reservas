package com.prueba.demo.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.prueba.demo.entity.Reserva_fecha;

@Repository
public class Reserva_fechaDaoOlmp implements Reserva_fechaDao {

	@Autowired
	private EntityManager entityManager;

	@Override
	public List<Reserva_fecha> findAll() {
		Session currentSession = entityManager.unwrap(Session.class);

		Query<Reserva_fecha> theQuery = currentSession.createQuery("from Reserva_fecha", Reserva_fecha.class);

		List<Reserva_fecha> reserva_fechas = theQuery.getResultList();

		return reserva_fechas;
	}

	@Override
	public Reserva_fecha findById(int id) {
		Session currentSession = entityManager.unwrap(Session.class);

		Reserva_fecha reserva_fecha = currentSession.get(Reserva_fecha.class, id);

		return reserva_fecha;
	}


	@Override
	public void save(Reserva_fecha reserva_fecha) {
		Session currentSession = entityManager.unwrap(Session.class);

		currentSession.saveOrUpdate(reserva_fecha);

	}

	@Override
	public void deleteById(int id) {
		Session currentSession = entityManager.unwrap(Session.class);

		Query<Reserva_fecha> theQuery = currentSession.createQuery("delete from Reserva_fecha where id=:idReserva_fecha", Reserva_fecha.class);

		theQuery.setParameter("idReserva_fecha", id);
		theQuery.executeUpdate();

	}






}