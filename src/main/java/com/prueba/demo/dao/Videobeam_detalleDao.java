package com.prueba.demo.dao;

import java.util.List;

import com.prueba.demo.entity.Videobeam_detalle;

public interface Videobeam_detalleDao {

	public List<Videobeam_detalle> findAll();

	public Videobeam_detalle findById(int id);


}