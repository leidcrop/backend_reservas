package com.prueba.demo.dao;

import java.util.List;

import com.prueba.demo.entity.Sedes;

public interface SedeDAO {

	public List<Sedes> findAll();

	public Sedes findById(int id);


}