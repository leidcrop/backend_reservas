package com.prueba.demo.dao;

import java.util.List;

import com.prueba.demo.entity.User;

public interface UserDao {

	public List<User> findAll();

	public User findById(int id);

	public void save(User user);

	public void deleteById(int id);

	public User findByLogin(String correo, String password);

}