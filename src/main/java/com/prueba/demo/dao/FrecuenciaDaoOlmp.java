package com.prueba.demo.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.prueba.demo.entity.Frecuencia;

@Repository
public class FrecuenciaDaoOlmp implements FrecuenciaDao {

	@Autowired
	private EntityManager entityManager;

	@Override
	public List<Frecuencia> findAll() {
		Session currentSession = entityManager.unwrap(Session.class);

		Query<Frecuencia> theQuery = currentSession.createQuery("from Frecuencia", Frecuencia.class);

		List<Frecuencia> frecuencia = theQuery.getResultList();

		return frecuencia;
	}

	@Override
	public Frecuencia findById(int id) {
		Session currentSession = entityManager.unwrap(Session.class);

		Frecuencia frecuencia = currentSession.get(Frecuencia.class, id);

		return frecuencia;
	}







}