package com.prueba.demo.dao;

import java.util.List;

import com.prueba.demo.entity.Reservas;

public interface ReservaDao {

	public List<Reservas> findAll();

	public Reservas findById(int id);
	
	
	public void save(Reservas reservas);

	public void deleteById(int id);


}