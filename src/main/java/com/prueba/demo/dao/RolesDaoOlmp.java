package com.prueba.demo.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.prueba.demo.entity.Roles;

@Repository
public class RolesDaoOlmp implements RolesDao {

	@Autowired
	private EntityManager entityManager;

	@Override
	public List<Roles> findAll() {
		Session currentSession = entityManager.unwrap(Session.class);

		Query<Roles> theQuery = currentSession.createQuery("from Roles", Roles.class);

		List<Roles> roles = theQuery.getResultList();

		return roles;
	}

	@Override
	public Roles findById(int id) {
		Session currentSession = entityManager.unwrap(Session.class);

		Roles roles = currentSession.get(Roles.class, id);

		return roles;
	}







}