package com.prueba.demo.dao;

import java.util.List;

import com.prueba.demo.entity.Roles;

public interface RolesDao {

	public List<Roles> findAll();

	public Roles findById(int id);


}