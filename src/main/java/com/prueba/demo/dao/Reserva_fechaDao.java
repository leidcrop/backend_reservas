package com.prueba.demo.dao;

import java.util.List;

import com.prueba.demo.entity.Reserva_fecha;

public interface Reserva_fechaDao {

	public List<Reserva_fecha> findAll();

	public Reserva_fecha findById(int id);

	
	public void save(Reserva_fecha reserva_fecha);

	public void deleteById(int id);



}