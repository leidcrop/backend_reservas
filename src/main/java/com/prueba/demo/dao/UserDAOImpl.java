package com.prueba.demo.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.prueba.demo.entity.User;

@Repository
public class UserDAOImpl implements UserDao {

	@Autowired
	private EntityManager entityManager;

	@Override
	public List<User> findAll() {
		Session currentSession = entityManager.unwrap(Session.class);

		Query<User> theQuery = currentSession.createQuery("from User", User.class);

		List<User> users = theQuery.getResultList();

		return users;
	}

	@Override
	public User findById(int id) {
		Session currentSession = entityManager.unwrap(Session.class);

		User user = currentSession.get(User.class, id);

		return user;
	}

	@Override
	public void save(User user) {
		Session currentSession = entityManager.unwrap(Session.class);

		currentSession.saveOrUpdate(user);

	}

	@Override
	public void deleteById(int id) {
		Session currentSession = entityManager.unwrap(Session.class);

		Query<User> theQuery = currentSession.createQuery("delete from User where id=:idUser", User.class);

		theQuery.setParameter("idUser", id);
		theQuery.executeUpdate();

	}

	@Override
	public User findByLogin(String correo, String password) {
		Session currentSession = entityManager.unwrap(Session.class);
		System.out.println(correo);
		System.out.println(password);
		System.out.println(" mirando si hasta aqui llegan las pinches variables");
		System.out.println(" mirando si hasta aqui llegan las pinches variables");

		System.out.println(" mirando si hasta aqui llegan las pinches variables");
		System.out.println(" mirando si hasta aqui llegan las pinches variables");
		System.out.println(" mirando si hasta aqui llegan las pinches variables");
		System.out.println(" mirando si hasta aqui llegan las pinches variables");
		System.out.println(" mirando si hasta aqui llegan las pinches variables");
		System.out.println(" mirando si hasta aqui llegan las pinches variables");


		Query<User> theQuery = currentSession
				.createQuery("SELECT u FROM User u WHERE correo = :correo AND password = :password", User.class);
		System.out.println("imrpimiendo usuario completo ");
		System.out.println("imrpimiendo usuario completo ");
		System.out.println("imrpimiendo usuario completo ");

		theQuery.setParameter("correo", correo);
		theQuery.setParameter("password", password);
		System.out.println(theQuery.getResultList());

		try {
			return theQuery.getSingleResult();
		} catch (NoResultException nre) {
			// TODO: handle exception
			throw new RuntimeException("Credenciales incorrectas");
		}

	}

}