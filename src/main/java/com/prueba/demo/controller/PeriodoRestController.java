package com.prueba.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import com.prueba.demo.entity.Periodos;
import com.prueba.demo.services.PeriodoService;

import java.util.Date;
import java.util.stream.Collectors;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.web.bind.annotation.RequestParam;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import net.bytebuddy.dynamic.loading.PackageDefinitionStrategy.Definition.Undefined;

//Indiciamos que es un controlador rest
@RestController
@RequestMapping("/api") // esta sera la raiz de la url, es decir http://127.0.0.1:8080/api/

public class PeriodoRestController {

	// Inyectamos el servicio para poder hacer uso de el
	@Autowired
	private PeriodoService periodoService;

	/*
	 * Este método se hará cuando por una petición GET (como indica la anotación) se
	 * llame a la url http://127.0.0.1:8080/api/users
	 */
	@GetMapping("/Periodos")
	public List<Periodos> findAll() {
		// retornará todos los usuarios
		return periodoService.findAll();
	}

	/*
	 * Este método se hará cuando por una petición GET  (como indica la anotación) se
	 * llame a la url + el id de un usuario http://127.0.0.1:8080/api/users/1
	 */
	@GetMapping("/Periodos/{Periodosid}")
	public Periodos getSalones(@PathVariable int Periodosid) {
		Periodos Periodos = periodoService.findById(Periodosid);

		if (Periodos == null) {
			throw new RuntimeException("Periodos no found-" + Periodosid);
		}
		// retornará al usuario con id pasado en la url
		return Periodos;
	}



	@DeleteMapping("/Periodos/{Periodosid}")
	public String detetePeriiodo(@PathVariable int periodoID) {

		Periodos periodos = periodoService.findById(periodoID);

		if (periodos == null) {
			throw new RuntimeException("PERIODO id not found -" + periodoID);
		}

		periodoService.deleteById(periodoID);

		// Esto método, recibira el id de un usuario por URL y se borrará de la bd.
		return "Deleted PERIODO id - " + periodoID;
	}

	@PostMapping("/Periodos/register")
	public Periodos addUser(@RequestBody Periodos periodo) {
		// Este metodo guardará al usuario enviado
		periodoService.save(periodo);
		return periodo;

	}




}