
package com.prueba.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import com.prueba.demo.entity.Videobeam;
import com.prueba.demo.services.VideobeamService;

import java.util.Date;
import java.util.stream.Collectors;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.web.bind.annotation.RequestParam;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import net.bytebuddy.dynamic.loading.PackageDefinitionStrategy.Definition.Undefined;

//Indiciamos que es un controlador rest
@RestController
@RequestMapping("/api") // esta sera la raiz de la url, es decir http://127.0.0.1:8080/api/

public class VideobeamRestController {

	// Inyectamos el servicio para poder hacer uso de el
	@Autowired
	private VideobeamService videobeamService;

	/*
	 * Este método se hará cuando por una petición GET (como indica la anotación) se
	 * llame a la url http://127.0.0.1:8080/api/users
	 */
	@GetMapping("/Videobeam")
	public List<Videobeam> findAll() {
		// retornará todos los usuarios
		return videobeamService.findAll();
	}

	/*
	 * Este método se hará cuando por una petición GET  (como indica la anotación) se
	 * llame a la url + el id de un usuario http://127.0.0.1:8080/api/users/1
	 */
	@GetMapping("/Videobeam/{Videobeamid}")
	public Videobeam getSalones(@PathVariable int Videobeamid) {
		Videobeam Videobeam = videobeamService.findById(Videobeamid);

		if (Videobeam == null) {
			throw new RuntimeException("salones no found-" + Videobeamid);
		}
		// retornará al usuario con id pasado en la url
		return Videobeam;
	}








}