package com.prueba.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import com.prueba.demo.entity.Frecuencia;
import com.prueba.demo.services.FrecuenciaService;

import java.util.Date;
import java.util.stream.Collectors;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.web.bind.annotation.RequestParam;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import net.bytebuddy.dynamic.loading.PackageDefinitionStrategy.Definition.Undefined;

//Indiciamos que es un controlador rest
@RestController
@RequestMapping("/api") // esta sera la raiz de la url, es decir http://127.0.0.1:8080/api/

public class FrecuenciaRestController {

	// Inyectamos el servicio para poder hacer uso de el
	@Autowired
	private FrecuenciaService frecuenciaService;

	/*
	 * Este método se hará cuando por una petición GET (como indica la anotación) se
	 * llame a la url http://127.0.0.1:8080/api/users
	 */
	@GetMapping("/frecuencia")
	public List<Frecuencia> findAll() {
		// retornará todos los usuarios
		return frecuenciaService.findAll();
	}

	/*
	 * Este método se hará cuando por una petición GET  (como indica la anotación) se
	 * llame a la url + el id de un usuario http://127.0.0.1:8080/api/users/1
	 */
	@GetMapping("/frecuencia/{frecuendiaid}")
	public Frecuencia getFrecuencia(@PathVariable int frecuendiaid) {
		Frecuencia frecuencia = frecuenciaService.findById(frecuendiaid);

		if (frecuencia == null) {
			throw new RuntimeException("frecuencia no found-" + frecuendiaid);
		}
		// retornará al usuario con id pasado en la url
		return frecuencia;
	}








}