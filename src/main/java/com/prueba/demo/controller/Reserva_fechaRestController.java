package com.prueba.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import com.prueba.demo.entity.Reserva_fecha;
import com.prueba.demo.services.Reserva_fechaService;

import java.util.Date;
import java.util.stream.Collectors;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.web.bind.annotation.RequestParam;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import net.bytebuddy.dynamic.loading.PackageDefinitionStrategy.Definition.Undefined;

//Indiciamos que es un controlador rest
@RestController
@RequestMapping("/api") // esta sera la raiz de la url, es decir http://127.0.0.1:8080/api/

public class Reserva_fechaRestController {

	// Inyectamos el servicio para poder hacer uso de el
	@Autowired
	private Reserva_fechaService reserva_fechaService;

	/*
	 * Este método se hará cuando por una petición GET (como indica la anotación) se
	 * llame a la url http://127.0.0.1:8080/api/users
	 */
	@GetMapping("/Reserva_fecha")
	public List<Reserva_fecha> findAll() {
		// retornará todos los usuarios
		return reserva_fechaService.findAll();
	}

	/*
	 * Este método se hará cuando por una petición GET  (como indica la anotación) se
	 * llame a la url + el id de un usuario http://127.0.0.1:8080/api/users/1
	 */
	@GetMapping("/Reserva_fecha/{Reserva_fechaid}")
	public Reserva_fecha getSalones(@PathVariable int Reserva_fechaid) {
		Reserva_fecha Reserva_fecha = reserva_fechaService.findById(Reserva_fechaid);

		if (Reserva_fecha == null) {
			throw new RuntimeException("Reserva_fecha no found-" + Reserva_fechaid);
		}
		// retornará al usuario con id pasado en la url
		return Reserva_fecha;
	}

	@DeleteMapping("/Reserva_fecha/{Reserva_fechaid}")
	public String deteteReservafecha(@PathVariable int reservaID) {

		Reserva_fecha reserva_fecha = reserva_fechaService.findById(reservaID);

		if (reserva_fecha == null) {
			throw new RuntimeException("RESERVA FECHA id not found -" + reservaID);
		}

		reserva_fechaService.deleteById(reservaID);

		// Esto método, recibira el id de un usuario por URL y se borrará de la bd.
		return "Deleted RESERVA FECHA id - " + reservaID;
	}

	@PostMapping("/Reserva_fecha/register")
	public Reserva_fecha addreservafechaa(@RequestBody Reserva_fecha reserva_fecha) {
		// Este metodo guardará al usuario enviado
		reserva_fechaService.save(reserva_fecha);
		return reserva_fecha;

	}






}