package com.prueba.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import com.prueba.demo.entity.Salones;
import com.prueba.demo.services.SalonesServices;

import java.util.Date;
import java.util.stream.Collectors;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.web.bind.annotation.RequestParam;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import net.bytebuddy.dynamic.loading.PackageDefinitionStrategy.Definition.Undefined;

//Indiciamos que es un controlador rest
@RestController
@RequestMapping("/api") // esta sera la raiz de la url, es decir http://127.0.0.1:8080/api/

public class SalonesRestController {

	// Inyectamos el servicio para poder hacer uso de el
	@Autowired
	private SalonesServices salonesServices;

	/*
	 * Este método se hará cuando por una petición GET (como indica la anotación) se
	 * llame a la url http://127.0.0.1:8080/api/users
	 */
	@GetMapping("/Salones")
	public List<Salones> findAll() {
		// retornará todos los usuarios
		return salonesServices.findAll();
	}

	/*
	 * Este método se hará cuando por una petición GET  (como indica la anotación) se
	 * llame a la url + el id de un usuario http://127.0.0.1:8080/api/users/1
	 */
	@GetMapping("/Salones/{Salonesid}")
	public Salones getSalones(@PathVariable int Salonesid) {
		Salones Salones = salonesServices.findById(Salonesid);

		if (Salones == null) {
			throw new RuntimeException("salones no found-" + Salonesid);
		}
		// retornará al usuario con id pasado en la url
		return Salones;
	}








}