package com.prueba.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import com.prueba.demo.entity.Reservas;
import com.prueba.demo.services.ReservaService;

import java.util.Date;
import java.util.stream.Collectors;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.web.bind.annotation.RequestParam;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import net.bytebuddy.dynamic.loading.PackageDefinitionStrategy.Definition.Undefined;

//Indiciamos que es un controlador rest
@RestController
@RequestMapping("/api") // esta sera la raiz de la url, es decir http://127.0.0.1:8080/api/

public class ReservaRestController {

	// Inyectamos el servicio para poder hacer uso de el
	@Autowired
	private ReservaService reservaService;

	/*
	 * Este método se hará cuando por una petición GET (como indica la anotación) se
	 * llame a la url http://127.0.0.1:8080/api/users
	 */
	@GetMapping("/Reservas")
	public List<Reservas> findAll() {
		// retornará todos los usuarios
		return reservaService.findAll();
	}

	/*
	 * Este método se hará cuando por una petición GET  (como indica la anotación) se
	 * llame a la url + el id de un usuario http://127.0.0.1:8080/api/users/1
	 */
	@GetMapping("/Reservas/{Reservasid}")
	public Reservas getSalones(@PathVariable int Reservasid) {
		Reservas Reservas = reservaService.findById(Reservasid);

		if (Reservas == null) {
			throw new RuntimeException("Reservas no found-" + Reservasid);
		}
		// retornará al usuario con id pasado en la url
		return Reservas;
	}



	@DeleteMapping("/Reservas/{Reservasid}")
	public String deteteUser(@PathVariable int reservaID) {

		Reservas reservas = reservaService.findById(reservaID);

		if (reservas == null) {
			throw new RuntimeException("reserva id not found -" + reservaID);
		}

		reservaService.deleteById(reservaID);

		// Esto método, recibira el id de un usuario por URL y se borrará de la bd.
		return "Deleted reserva id - " + reservaID;
	}

	@PostMapping("/Reservas/register")
	public Reservas addUser(@RequestBody Reservas reservas) {
		// Este metodo guardará al usuario enviado
		reservaService.save(reservas);
		return reservas;

	}




}