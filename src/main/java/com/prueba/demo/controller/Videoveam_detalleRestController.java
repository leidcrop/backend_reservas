package com.prueba.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import com.prueba.demo.entity.Videobeam_detalle;
import com.prueba.demo.services.Videobeam_detalleService;

import java.util.Date;
import java.util.stream.Collectors;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.web.bind.annotation.RequestParam;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import net.bytebuddy.dynamic.loading.PackageDefinitionStrategy.Definition.Undefined;

//Indiciamos que es un controlador rest
@RestController
@RequestMapping("/api") // esta sera la raiz de la url, es decir http://127.0.0.1:8080/api/

public class Videoveam_detalleRestController {

	// Inyectamos el servicio para poder hacer uso de el
	@Autowired
	private Videobeam_detalleService videobeam_detalleService;

	/*
	 * Este método se hará cuando por una petición GET (como indica la anotación) se
	 * llame a la url http://127.0.0.1:8080/api/users
	 */
	@GetMapping("/Videobeam_detalle")
	public List<Videobeam_detalle> findAll() {
		// retornará todos los usuarios
		return videobeam_detalleService.findAll();
	}

	/*
	 * Este método se hará cuando por una petición GET  (como indica la anotación) se
	 * llame a la url + el id de un usuario http://127.0.0.1:8080/api/users/1
	 */
	@GetMapping("/Videobeam_detalle/{Videobeam_detalleid}")
	public Videobeam_detalle getSalones(@PathVariable int Videobeam_detalleid) {
		Videobeam_detalle Videobeam_detalle = videobeam_detalleService.findById(Videobeam_detalleid);

		if (Videobeam_detalle == null) {
			throw new RuntimeException("salones no found-" + Videobeam_detalleid);
		}
		// retornará al usuario con id pasado en la url
		return Videobeam_detalle;
	}








}







